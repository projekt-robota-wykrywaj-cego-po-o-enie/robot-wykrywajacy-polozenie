# Robot wykrywający położenie

Opis projektu

Celem projektu jest zbudowanie i zaprogramowanie robota określającego swoje położenie na powierzchni. Pojazd będzie komunikował się za pomocą sieci wifi z serwerem, z którego będzie pobierana mapa pomieszczenia, w którym się znajduje. Położenie wyznaczane będzie za pomocą 5 czujników ultradźwiękowych, a napęd będzie realizowany przez dwa silniki DC. 

Projekt będzie zbudowany na platformie arduino UNO, a komunikacja wifi będzie się odbywała przez moduł ESP32. CZujniki odległosći to HY-SRF05.

dbDiagram:

![](dbDiagram.jpg)

